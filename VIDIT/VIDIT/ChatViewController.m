//
//  ChatViewController.m
//  VIDIT
//
//  Created by brst on 5/6/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ChatViewController.h"
#import "LeftTableViewCell.h"
#import "RightTableViewCell.h"

@interface ChatViewController ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    UISegmentedControl *segmentedControl;
    UIImageView *friendImg;
    UIImageView *myImg;
    UIButton *changeCameraBtn,*endCallBtn;
    UIView *chatView,*videoCallingView;
    UIView *messageView;
    UITextView *msgTextView;
    UIImageView *mikeImg, *cameraImg;
    UIButton *send,*cameraBtn,*mikeBtn;
    CGPoint touchStart;

}
@end

static NSString *leftCellIdentifier = @"LeftCell";
static NSString *rightCellIdentifier = @"RightCell";
@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Chats";
    self.view.backgroundColor=[UIColor whiteColor];
    UIBarButtonItem *addUser=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"user"] style:UIBarButtonItemStylePlain target:self action:@selector(addUserBtnPressed)];
    self.navigationItem.rightBarButtonItem=addUser;
    NSArray *itemArray = [NSArray arrayWithObjects: @"Chat", @"Call", @"Video", nil];
    
    segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    [segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
    segmentedControl.selectedSegmentIndex = 1;
    segmentedControl.tintColor=[UIColor blackColor];
    [self.view addSubview:segmentedControl];
    
    // Set divider images
    [segmentedControl setDividerImage:[UIImage imageNamed:@"divider"]forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

    UIImage *segmentSelected =[[UIImage imageNamed:@"selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    UIImage *segmentUnselected = [[UIImage imageNamed:@"whiteBox"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5 )];
    
    [segmentedControl setBackgroundImage:segmentUnselected forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segmentedControl setBackgroundImage:segmentSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [segmentedControl setTitleTextAttributes:@{ NSForegroundColorAttributeName: [UIColor colorWithRed:(4/255.0f) green:(173/255.0f) blue:(229/255.0f) alpha:1.0f]} forState:UIControlStateSelected];
    [self chatScreen];
    [self videoCallingScreen];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    NSLog(@"index=%ld",(long)segment.selectedSegmentIndex);
    [msgTextView resignFirstResponder];
    switch (segment.selectedSegmentIndex ) {
        case 0:
            chatView.hidden=NO;
            videoCallingView.hidden=YES;
            break;
        case 1:
            chatView.hidden=YES;
            videoCallingView.hidden=NO;
            break;
        case 2:
            chatView.hidden=YES;
            videoCallingView.hidden=NO;
            break;
            
        default:
            break;
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)addUserBtnPressed
{
}

#pragma mark Video Calling Segment

-(void)videoCallingScreen
{
    videoCallingView=[[UIView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height)];
    videoCallingView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:videoCallingView];
    
    friendImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100)];
    friendImg.image=[UIImage imageNamed:@"friendimg"];
    [videoCallingView addSubview:friendImg];
    
    myImg=[[UIImageView alloc]initWithFrame:CGRectMake(20,10, 80, 100)];
    myImg.image=[UIImage imageNamed:@"myImg"];
    [videoCallingView addSubview:myImg];
    
    endCallBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-50, 10, 35, 25)];
    endCallBtn.backgroundColor=[UIColor blackColor];
    endCallBtn.layer.opacity=0.5;
    endCallBtn.layer.cornerRadius=10;
    [endCallBtn setImage:[UIImage imageNamed:@"frontCamera"] forState:UIControlStateNormal];
    [videoCallingView addSubview:endCallBtn];
    
    changeCameraBtn=[[UIButton alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height-150, self.view.frame.size.width-40, 40)];
    changeCameraBtn.backgroundColor=[UIColor redColor];
    [changeCameraBtn setImage:[UIImage imageNamed:@"whitePhone"] forState:UIControlStateNormal];
    [videoCallingView addSubview:changeCameraBtn];
    
}
#pragma  mark- Touch Events
  //To hide keyboard by swaping  message view to down

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    touchStart = [touch locationInView:self.view];
    NSLog(@"touchStart=%f touchStart=%f",touchStart.x,touchStart.y);
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint touchLocation = [[touches anyObject] locationInView:self.view];
    NSLog(@"touchLocation=%f touchLocation=%f",touchLocation.x,touchLocation.y);
    if (CGRectContainsPoint(messageView.frame, touchLocation)) {
        if(touchLocation.y>touchStart.y)
        {
            [msgTextView resignFirstResponder];
            
        }
        
        return;
        
    }
}


#pragma mark- Chatting Segment

-(void)chatScreen
{
    chatView=[[UIView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height)];
    chatView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:chatView];
    
    UITableView *chatTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-155)];
    //chatTableView.backgroundColor=[UIColor grayColor];
    [chatView addSubview:chatTableView];
    [chatTableView registerClass:[LeftTableViewCell class] forCellReuseIdentifier:leftCellIdentifier];
    chatTableView.delegate=self;
    chatTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    chatTableView.dataSource=self;
    

    
    messageView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-155, self.view.frame.size.width, 50)];
    messageView.backgroundColor=[UIColor colorWithRed:(248/255.0f) green:(248/255.0f) blue:(248/255.0f) alpha:1.0];
    messageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    messageView.layer.borderWidth=0.3f;
    [chatView addSubview:messageView];
    
    UIImageView *stickerImg=[[UIImageView alloc]initWithFrame:CGRectMake(5, messageView.frame.size.height-35, 20, 20)];
    stickerImg.image=[UIImage imageNamed:@"stickerImg"];
    [messageView addSubview:stickerImg];
    
    UIButton *stickerBtn=[[UIButton alloc]init];
    stickerBtn.frame=CGRectMake(0, 0, 30, 40);
    stickerBtn.backgroundColor=[UIColor clearColor];
    [messageView addSubview:stickerBtn];
    
    msgTextView=[[UITextView alloc]initWithFrame:CGRectMake(stickerBtn.frame.size.width, 10, self.view.frame.size.width-110, 30)];
    msgTextView.layer.cornerRadius=10;
    msgTextView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    msgTextView.layer.borderWidth=0.5f;
    msgTextView.delegate=self;
    msgTextView.autocorrectionType=UITextAutocapitalizationTypeNone;
    [messageView addSubview:msgTextView];
    
    cameraImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-70, 15, 30, 20)];
    cameraImg.image=[UIImage imageNamed:@"camera2"];
    [messageView addSubview:cameraImg];
    
    cameraBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    cameraBtn.frame=CGRectMake(self.view.frame.size.width-60,0, 30, 40);
    cameraBtn.backgroundColor=[UIColor clearColor];
    //[cameraBtn addTarget:self action:@selector(cameraBtnPressed) forControlEvents:UIControlEventTouchDragInside];
    [messageView addSubview:cameraBtn];
    
    mikeImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-25, 15, 15, 20)];
    mikeImg.image=[UIImage imageNamed:@"mike"];
    [messageView addSubview:mikeImg];
    
    mikeBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    mikeBtn.frame=CGRectMake(self.view.frame.size.width-30,0, 30, 40);
    mikeBtn.backgroundColor=[UIColor clearColor];
    [messageView addSubview:mikeBtn];
    
    send=[UIButton buttonWithType:UIButtonTypeSystem];
    send.frame=CGRectMake(self.view.frame.size.width-60,messageView.frame.size.height-45, 45, 40);
    [send setTitle:@"Send" forState:UIControlStateNormal];
    [send setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    send.titleLabel.font=[UIFont boldSystemFontOfSize:16];
    send.backgroundColor=[UIColor clearColor];
    send.hidden=YES;
    [send addTarget:self action:@selector(sendBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [messageView addSubview:send];
    


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 12;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell1;
    if(indexPath.row%2==0)
    {
        LeftTableViewCell *cell = (LeftTableViewCell *)[tableView dequeueReusableCellWithIdentifier:leftCellIdentifier];
        if(cell==nil)
        {
            cell=[[LeftTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:leftCellIdentifier];
            
        }
        cell.leftImage.image=[UIImage imageNamed:@"2"];
        cell.msgLbl.text=@"Lorem ipsum dolor,consectetuer adipiscing elit,sed diam nonum.";
        cell.selectionStyle=UITableViewCellSeparatorStyleNone;
        cell.timeLbl.text=@"5:22 pm";
        return  cell;
    }
    else
    {
        RightTableViewCell *cell = (RightTableViewCell *)[tableView dequeueReusableCellWithIdentifier:rightCellIdentifier];
        if(cell==nil)
        {
            cell=[[RightTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rightCellIdentifier];
            
        }
        cell.msgLbl.text=@"Lorem ipsum dolor,consectetuer adipiscing elit,sed diam nonum.";
        cell.selectionStyle=UITableViewCellSeparatorStyleNone;
        cell.timeLbl.text=@"5:22 pm";
        return  cell;
    }
    return cell1;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{

    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
       return YES;
}
- (void)textViewDidChange:(UITextView *)textView
{
    if(msgTextView.text.length==0)
    {
        send.hidden=YES;
        cameraImg.hidden=NO;
        mikeImg.hidden=NO;
    }
    else
    {
        send.hidden=NO;
        cameraImg.hidden=YES;
        mikeImg.hidden=YES;
    }

}

-(void)showKeyboard:(NSNotification *)notify
{
    
    CGRect keyboardBounds;
    NSDictionary *info = [notify userInfo];
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSLog(@"%@",NSStringFromCGRect(messageView.frame));
    
    messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y-keyboardBounds.size.height, messageView.frame.size.width, messageView.frame.size.height);
  
}
-(void)hideKeyboard:(NSNotification *)notify{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    CGFloat windowHeight = [UIApplication sharedApplication].keyWindow.frame.size.height;
    messageView.frame = CGRectMake(messageView.frame.origin.x, windowHeight - messageView.frame.size.height-100 , messageView.frame.size.width, messageView.frame.size.height);
    [UIView commitAnimations];
}

-(void)sendBtnPressed:(UIButton*)btn
{
    msgTextView.text=@"";
    send.hidden=YES;
    cameraImg.hidden=NO;
    mikeImg.hidden=NO;
    [msgTextView resignFirstResponder];
    
}

@end
