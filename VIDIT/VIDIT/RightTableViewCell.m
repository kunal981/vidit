//
//  RightTableViewCell.m
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "RightTableViewCell.h"

@implementation RightTableViewCell
@synthesize rightImage,timeLbl,msgLbl,rightMsg;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        rightImage=[[UIImageView alloc]initWithFrame:CGRectMake(self.contentView.frame.size.width-200,20, 200, 40)];
        rightImage.image=[UIImage imageNamed:@"rightBubble"];
        [self addSubview:rightImage];
        
        msgLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 40)];
        msgLbl.font=[UIFont systemFontOfSize:12];
        msgLbl.textColor=[UIColor grayColor];
        msgLbl.numberOfLines=0;
        [rightImage addSubview:msgLbl];
        
        timeLbl=[[UILabel alloc]initWithFrame:CGRectMake(self.contentView.frame.size.width-240, 20, 40, 40)];
        timeLbl.font=[UIFont systemFontOfSize:10];
        timeLbl.numberOfLines=1;
        timeLbl.textColor=[UIColor grayColor];
        [self addSubview:timeLbl];

    }
    return self;
}

@end
