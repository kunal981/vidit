//
//  ProfileViewController.m
//  VIDIT
//
//  Created by brst on 4/18/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()
{
    UIScrollView *scroll;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    imagesArray=[[NSMutableArray alloc]init];
    self.view.backgroundColor=[UIColor whiteColor];
    self.navigationItem.title=@"Profile";
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    UIBarButtonItem *addUser=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"user"] style:UIBarButtonItemStylePlain target:self action:@selector(addUserBtnPressed)];
    self.navigationItem.rightBarButtonItem=addUser;

    
    [self profileView];
    
    
}

-(void)profileView
{
    viewProfile=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 170)];
    viewProfile.backgroundColor=[UIColor colorWithRed:(4/255.0f) green:(173/255.0f) blue:(229/255.0f) alpha:1.0f];
    [self.view addSubview:viewProfile];
    
    profileImg=[[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-100)/2, 0, 100, 100)];
    profileImg.image=[UIImage imageNamed:@"pf.png"];
    profileImg.backgroundColor=[UIColor clearColor];
    profileImg.layer.borderWidth=3;
    profileImg.layer.cornerRadius=profileImg.frame.size.width/2;
    profileImg.layer.borderColor=[UIColor colorWithRed:(93/255.0f) green:(210/255.0f) blue:(255/255.0f) alpha:1.0f].CGColor;
    [self.view addSubview:profileImg];
    
    name=[[UILabel alloc]initWithFrame:CGRectMake(0,profileImg.frame.size.height+profileImg.frame.origin.y+10, self.view.frame.size.width,20)];
    name.text=@"Rob Villiams";
    name.textColor=[UIColor whiteColor];
    name.textAlignment=NSTextAlignmentCenter;
    name.font=[UIFont boldSystemFontOfSize:18];
    [viewProfile addSubview:name];
    
    myProfileLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, name.frame.size.height+name.frame.origin.y+10,  self.view.frame.size.width, 20)];
    myProfileLbl.textColor=[UIColor whiteColor];
    myProfileLbl.text=@"Hey there! I am using vidit messenger";
    myProfileLbl.font=[UIFont boldSystemFontOfSize:12];
    myProfileLbl.textAlignment=NSTextAlignmentCenter;
    [viewProfile addSubview:myProfileLbl];
    
    scroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0,viewProfile.frame.origin.y+viewProfile.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-180)];
    scroll.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:scroll];

    [self imageSection];
    [self videoSection];
    [self phoneView];
    
}
-(void)imageSection
{
    imageSectionView=[[UIView alloc]initWithFrame:CGRectMake(0,10, self.view.frame.size.width, 95)];
    imageSectionView.backgroundColor=[UIColor clearColor];
    [scroll addSubview:imageSectionView];
    
    
    lblImage=[[UILabel alloc]initWithFrame:CGRectMake(60, 5, self.view.frame.size.width-60,20)];
    lblImage.text=@"IMAGES";
    lblImage.textColor=[UIColor lightGrayColor];
    lblImage.textAlignment=NSTextAlignmentLeft;
    lblImage.font=[UIFont boldSystemFontOfSize:14];
    [imageSectionView addSubview:lblImage];

    
    UIView * lineView=[[UIView alloc]initWithFrame:CGRectMake(10, lblImage.frame.size.height+lblImage.frame.origin.y+5, self.view.frame.size.width-10,0.5)];
    lineView.backgroundColor=[UIColor lightGrayColor];
    [imageSectionView addSubview:lineView];
   
  UIView *  lineView1=[[UIView alloc]initWithFrame:CGRectMake(50, 5, 0.5,  lblImage.frame.size.height+lblImage.frame.origin.y)];
    lineView1.backgroundColor=[UIColor lightGrayColor];
    [imageSectionView addSubview:lineView1];
    
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(25,7, 15, 15)];
    imageView.image=[UIImage imageNamed:@"camera"];
    [imageSectionView addSubview:imageView];
    
    //UISCROLL VIEW
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,lineView.frame.size.height+lineView.frame.origin.y+5,self.view.frame.size.width ,70)];
    scrollView.backgroundColor=[UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled=YES;
    [imageSectionView addSubview:scrollView];
    
    int x=10;
   
    for (int i=0; i<8; i++)
    {
        UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(x,0, 60,60)];
        imageView.backgroundColor=[UIColor clearColor];
        imageView.image=[UIImage imageNamed:@"image"];
        x+=65;
        [scrollView addSubview:imageView];
    }
    scrollView.contentSize=CGSizeMake(x, 70);

}

-(void)videoSection
{
    videoSectionView=[[UIView alloc]initWithFrame:CGRectMake(0, imageSectionView.frame.size.height+imageSectionView.frame.origin.y+5, self.view.frame.size.width, 95)];
    videoSectionView.backgroundColor=[UIColor clearColor];
    [scroll addSubview:videoSectionView];
    
    
    lblImage=[[UILabel alloc]initWithFrame:CGRectMake(60, 5, self.view.frame.size.width-60,20)];
    lblImage.text=@"VIDEOS";
    lblImage.textColor=[UIColor lightGrayColor];
    lblImage.textAlignment=NSTextAlignmentLeft;
    lblImage.font=[UIFont boldSystemFontOfSize:14];
    [videoSectionView addSubview:lblImage];
    
    
    UIView * lineView=[[UIView alloc]initWithFrame:CGRectMake(10, lblImage.frame.size.height+lblImage.frame.origin.y+5, self.view.frame.size.width-10, .5)];
    lineView.backgroundColor=[UIColor lightGrayColor];
    [videoSectionView addSubview:lineView];
    
    UIView *  lineView1=[[UIView alloc]initWithFrame:CGRectMake(50, 5, 0.5,  lblImage.frame.size.height+lblImage.frame.origin.y)];
    lineView1.backgroundColor=[UIColor lightGrayColor];
    [videoSectionView addSubview:lineView1];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(25,10, 15, 10)];
    imageView.image=[UIImage imageNamed:@"video"];
    [videoSectionView addSubview:imageView];
    
    //UISCROLL VIEW
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,lineView.frame.size.height+lineView.frame.origin.y+5,self.view.frame.size.width ,70)];
    scrollView.backgroundColor=[UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
   
    [videoSectionView addSubview:scrollView];
    
    
    
    // int row=(imagesArray.count/2+imagesArray.count%2);
    int x=10;
    
    for (int i=0; i<8; i++)
    {
        UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(x,0, 60,60)];
        imageView.image=[UIImage imageNamed:@"image"];
        x+=65;
        [scrollView addSubview:imageView];
    }
    scrollView.contentSize=CGSizeMake(x, 70);
    
    

}
-(void)phoneView
{
    phoneView=[[UIView alloc]initWithFrame:CGRectMake(0, videoSectionView.frame.size.height+videoSectionView.frame.origin.y+5, self.view.frame.size.width, 95)];
    //phoneView.backgroundColor=[UIColor grayColor];
    
    [scroll addSubview:phoneView];
    
    
    lblImage=[[UILabel alloc]initWithFrame:CGRectMake(60, 5, self.view.frame.size.width-60,20)];
    lblImage.text=@"PHONE";
    lblImage.textColor=[UIColor lightGrayColor];
    lblImage.textAlignment=NSTextAlignmentLeft;
    lblImage.font=[UIFont boldSystemFontOfSize:14];
    [phoneView addSubview:lblImage];
    
    
    UIView * lineView=[[UIView alloc]initWithFrame:CGRectMake(10, lblImage.frame.size.height+lblImage.frame.origin.y+5, self.view.frame.size.width-10, .5)];
    lineView.backgroundColor=[UIColor lightGrayColor];
    [phoneView addSubview:lineView];
    
    UIView *  lineView1=[[UIView alloc]initWithFrame:CGRectMake(50, 5, .5,  lblImage.frame.size.height+lblImage.frame.origin.y)];
    lineView1.backgroundColor=[UIColor lightGrayColor];
    [phoneView addSubview:lineView1];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(25,7, 15, 15)];
    imageView.image=[UIImage imageNamed:@"phone"];
    [phoneView addSubview:imageView];

    lblPhoneNo=[[UILabel alloc]initWithFrame:CGRectMake(10, lineView.frame.size.height+lineView.frame.origin.y+5, self.view.frame.size.width-60,20)];
    lblPhoneNo.text=@"+91 7696800492";
    lblPhoneNo.textColor=[UIColor blackColor];
    lblPhoneNo.textAlignment=NSTextAlignmentLeft;
    lblPhoneNo.font=[UIFont systemFontOfSize:12];
    [phoneView addSubview:lblPhoneNo];
    
    scroll.contentSize=CGSizeMake(self.view.frame.size.width, phoneView.frame.origin.y +phoneView.frame.size.height);

    
}
-(void)addUserBtnPressed
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
