//
//  ViewController.h
//  VIDIT
//
//  Created by brst on 4/4/15.
//  Copyright (c) 2015 brst. All rights reserved.
//




@interface LoginViewController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    
    IBOutlet UILabel *countryCode;
    IBOutlet UILabel *countryName;
    IBOutlet UITextField *phnNo;
    SWRevealViewController *viewController;
    UIPickerView *countryDataPicker;
    IBOutlet UIButton *countryBtn;
    IBOutlet UIView *loginView;
}

- (IBAction)doneBtnPressed:(id)sender;
- (IBAction)showCountryList:(id)sender;

@end

