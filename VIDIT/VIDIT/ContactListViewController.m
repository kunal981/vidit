//
//  ContactListViewController.m
//  VIDIT
//
//  Created by brst on 4/6/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ContactListViewController.h"
#import "ContactCell.h"


@interface ContactListViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate>
{
    UISearchBar *searchBar;
    UITableView *contactTableView;
    ChatViewController *chatObj;
    NSArray *contactList;
    NSMutableArray *filteredArr;
    UISearchDisplayController *searchDisplayController;
}

@end
static NSString *contactCellIdentifier = @"contactCell";
@implementation ContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self topBar];
    
    
    
    searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    searchBar.placeholder=@"Search";
    //searchBar.userInteractionEnabled=NO;
    searchBar.delegate=self;

    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    [searchDisplayController setSearchResultsDelegate:self];
    [searchBar setShowsScopeBar:YES];
    
    contactTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height-64)];
    contactTableView.tableHeaderView=searchBar;
    contactTableView.delegate=self;
    contactTableView.dataSource=self;
    contactTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [contactTableView registerClass:[ContactCell class] forCellReuseIdentifier:contactCellIdentifier];
  

    [self.view addSubview:contactTableView];
    contactList=@[@"Jessica Laine",@"John Burg",@"David Clarke",@"Stalen Ken",@"David Clarke",@"Stuart Mash"];
    filteredArr = [NSMutableArray arrayWithCapacity:[contactList count]];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [filteredArr count];
    } else {
        return [contactList count];
    }
    return contactList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    ContactCell *cell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
    if(cell==nil)
    {
        cell=[[ContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:contactCellIdentifier];
        
    }
    cell.userImg.image=[UIImage imageNamed:@"facePic"];
    cell.userPlace.text=@"New york,USA";
    cell.time.text=@"5.22 pm";
    cell.msgCount.text=@" 20";
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell.userName.text = [filteredArr objectAtIndex:indexPath.row];
    } else {
        cell.userName.text=contactList[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    chatObj =[[ChatViewController alloc]init];
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationController pushViewController:chatObj animated:YES];
}

-(void)topBar
{
    
    self.navigationController.navigationBarHidden=NO;
    self.title=@"Messages";
    
    UIBarButtonItem *addUser=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"user"] style:UIBarButtonItemStylePlain target:self action:@selector(addUserBtnPressed)];
    self.navigationItem.rightBarButtonItem=addUser;
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)menuBtnPressed
{
    
    
//    NSLog(@"menuBtnPressed");
//    CGRect destination = self.navigationController.view.frame;
//    
//    
//    if (destination.origin.x > 0)
//    {
//        destination.origin.x = 0;
//    }else
//    {
//        destination.origin.x = 240;
//    }
//    
//    [UIView animateWithDuration:0.25 animations:^
//     {
//         self.navigationController.view.frame = destination;
//         //[self presentViewController:menu animated:YES completion:nil];
//         
//     }];

    
}
-(void)addUserBtnPressed
{
   
}
#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [filteredArr removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    filteredArr = [NSMutableArray arrayWithArray:[contactList filteredArrayUsingPredicate:predicate]];
    [searchDisplayController.searchResultsTableView reloadData];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes

    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [contactTableView reloadData];
}
@end
