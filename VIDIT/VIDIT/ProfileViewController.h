//
//  ProfileViewController.h
//  VIDIT
//
//  Created by brst on 4/18/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface ProfileViewController : UIViewController
{
    
    UIView * viewProfile;
    UIView * imageSectionView;
    UIView * videoSectionView ,*phoneView;
    
    UIImageView* profileImg;
    UILabel *  lblPhoneNo;
    UILabel *name,*myProfileLbl,*lblImage;
    UIScrollView * scrollView;
    NSMutableArray *imagesArray;
        
 }

@end
