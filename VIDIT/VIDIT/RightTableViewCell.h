//
//  RightTableViewCell.h
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightTableViewCell : UITableViewCell
{
    UIImageView *rightImage;
    UIImageView *rightMsg;
    UILabel *msgLbl,*timeLbl;
}
@property(nonatomic,strong)UIImageView *rightImage;
@property(nonatomic,strong)UIImageView *rightMsg;
@property(nonatomic,strong)UILabel *msgLbl,*timeLbl;

@end
