//
//  LeftTableViewCell.h
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftTableViewCell : UITableViewCell
{
    UIImageView *leftImage;
    UIImageView *leftMsg;
    UILabel *msgLbl,*timeLbl;
}
@property(nonatomic,strong)UIImageView *leftImage;
@property(nonatomic,strong)UIImageView *leftMsg;
@property(nonatomic,strong)UILabel *msgLbl,*timeLbl;
@end
