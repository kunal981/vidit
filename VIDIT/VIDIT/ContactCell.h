//
//  ContactCell.h
//  VIDIT
//
//  Created by brst on 5/18/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
{
    UIImageView *userImg;
    UILabel *msgCount,*userName,*time,*userPlace;
}
@property(nonatomic,strong)UIImageView *userImg;
@property(nonatomic,strong)UILabel *msgCount,*userName,*time,*userPlace;
@end
