//
//  MenuViewController.m
//  VIDIT
//
//  Created by brst on 4/6/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "MenuViewController.h"
#import "ContactListViewController.h"
#import "SettingViewController.h"
#import "ProfileViewController.h"
@interface MenuViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *profileImg;
    UILabel *name,*myProfileLbl;
    UITableView *menuOptionTable;
    NSArray *subMenu;
    NSArray *imgArr;

}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithRed:(26/255.0f) green:(117/255.0f) blue:(187/255.0f) alpha:1.0f];
    subMenu=@[@"Profile",@"Chat",@"Groups",@"Invite friends",@"Settings"];
    imgArr=@[@"contact_white",@"contact_white",@"group_white",@"invite_white",@"setting_white"];
    
    profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 80, 60, 60)];
    profileImg.image=[UIImage imageNamed:@"pf"];
    [self.view addSubview:profileImg];
    
    name=[[UILabel alloc]initWithFrame:CGRectMake(80,100, 100, 20)];
    name.text=@"Rob Villiams";
    name.textColor=[UIColor whiteColor];
    name.font=[UIFont boldSystemFontOfSize:14];
    [self.view addSubview:name];
    
    myProfileLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 140, 100, 20)];
    myProfileLbl.textColor=[UIColor whiteColor];
    myProfileLbl.text=@"My Profile";
    myProfileLbl.font=[UIFont boldSystemFontOfSize:16];
    [self.view addSubview:myProfileLbl];
    
    menuOptionTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 180, self.view.frame.size.width, self.view.frame.size.height-180)];
    menuOptionTable.delegate=self;
    menuOptionTable.dataSource=self;
    menuOptionTable.scrollEnabled=YES;
    menuOptionTable.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    menuOptionTable.backgroundColor=[UIColor clearColor];
    menuOptionTable.separatorColor=[UIColor colorWithRed:(10/255.0f) green:(100/255.0f) blue:(169/255.0f) alpha:1.0f];
    [self.view addSubview:menuOptionTable];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 60;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    
    cell.imageView.image=[UIImage imageNamed:imgArr[indexPath.row]];
    cell.textLabel.text=subMenu[indexPath.row];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.font=[UIFont boldSystemFontOfSize:16];
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    NSInteger row = indexPath.row;
    NSLog(@"%ld",(long)row);
    
    
    if (row == 0)
    {
        // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
        if ( ![frontNavigationController.topViewController isKindOfClass:[ProfileViewController class]] )
        {
            ProfileViewController *firstVC = [[ProfileViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:firstVC];
            [revealController pushFrontViewController:navigationController animated:YES];
        }
        // Seems the user attempts to 'switch' to exactly the same controller he came from!
        else
        {
            [revealController revealToggle:self];
        }
    }

    if (row == 4)
    {
        // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
        if ( ![frontNavigationController.topViewController isKindOfClass:[SettingViewController class]] )
        {
            SettingViewController *firstVC = [[SettingViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:firstVC];
            [revealController pushFrontViewController:navigationController animated:YES];
        }
        // Seems the user attempts to 'switch' to exactly the same controller he came from!
        else
        {
            [revealController revealToggle:self];
        }
    }
    if (row == 1)
    {
        // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
        if ( ![frontNavigationController.topViewController isKindOfClass:[ContactListViewController class]] )
        {
            ContactListViewController *firstVC = [[ContactListViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:firstVC];
            [revealController pushFrontViewController:navigationController animated:YES];
        }
        // Seems the user attempts to 'switch' to exactly the same controller he came from!
        else
        {
            [revealController revealToggle:self];
        }
    }
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
