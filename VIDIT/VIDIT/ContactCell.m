//
//  ContactCell.m
//  VIDIT
//
//  Created by brst on 5/18/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell
@synthesize msgCount,userName,time,userPlace,userImg;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        userImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 70,70 )];
       
        userImg.layer.cornerRadius=userImg.frame.size.width/2;
        [self addSubview:userImg];
        
        userName=[[UILabel alloc]initWithFrame:CGRectMake(userImg.frame.size.width+20, 10, self.frame.size.width-userImg.frame.size.width-20, 20)];
        
        userName.font=[UIFont systemFontOfSize:14];
        [self addSubview:userName];
        
        userPlace=[[UILabel alloc]initWithFrame:CGRectMake(userImg.frame.size.width+20, 30, self.frame.size.width-userImg.frame.size.width-20, 20)];
        userPlace.textColor=[UIColor grayColor];
        userPlace.font=[UIFont systemFontOfSize:10];
        [self addSubview:userPlace];
        
        time=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-80, 20,50, 20)];
        
        time.textColor=[UIColor grayColor];
        time.font=[UIFont systemFontOfSize:10];
        [self addSubview:time];
        
        msgCount=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-15, 45,15, 15)];
        
        msgCount.backgroundColor=[UIColor colorWithRed:(4/255.0f) green:(173/255.0f) blue:(229/255.0f) alpha:1.0f];
        msgCount.textColor=[UIColor whiteColor];
        msgCount.font=[UIFont systemFontOfSize:10];
        msgCount.numberOfLines = 1;
        msgCount.adjustsFontSizeToFitWidth = YES;
        [self addSubview:msgCount];

        
    }
    return self;
}
@end
