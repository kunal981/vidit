//
//  ViewController.m
//  VIDIT
//
//  Created by brst on 4/4/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "LoginViewController.h"




@interface LoginViewController ()<SWRevealViewControllerDelegate>
{
    ContactListViewController *contactObj;
    NSMutableArray *sortedCountryArray;
    NSArray *countryArray;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    countryDataPicker=[[UIPickerView alloc]initWithFrame:countryBtn.bounds];
    countryDataPicker.delegate=self;
    countryDataPicker.dataSource=self;
    [loginView addSubview:countryDataPicker];
    countryDataPicker.hidden=YES;
    countryDataPicker.backgroundColor=[UIColor whiteColor];
    
    NSLocale *locale = [NSLocale currentLocale];
    countryArray = [NSLocale ISOCountryCodes];
    sortedCountryArray = [[NSMutableArray alloc] init];
    for (NSString *countryCode1 in countryArray) {
        
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode1];
        [sortedCountryArray addObject:displayNameString];
        
    }
    
    NSLog(@"count =%lu sortedCountryArray=%@ ",(unsigned long)sortedCountryArray.count,countryArray);
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneBtnPressed:(id)sender {
   // contactObj =[[ContactListViewController alloc]init];
   // [self.navigationController pushViewController:contactObj animated:YES];

    contactObj = [[ContactListViewController alloc] init];
    MenuViewController *rearViewController = [[MenuViewController alloc] init];
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:contactObj];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    revealController.delegate = self;
    
    viewController = revealController;
    
    [self.navigationController pushViewController:viewController animated:YES];

   // UINavigationController *root=[[UINavigationController alloc]initWithRootViewController:rearViewController];
    
   

}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frmae=self.view.frame;
    frmae.origin.y -=100;
    [self.view setFrame:frmae];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frmae=self.view.frame;
    frmae.origin.y +=100;
    [self.view setFrame:frmae];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [ textField resignFirstResponder];
    return YES;
}

- (IBAction)showCountryList:(id)sender {
    [phnNo resignFirstResponder];
    countryDataPicker.hidden=NO;
}



// The number of columns of data
- (NSInteger )numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return sortedCountryArray.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
    return sortedCountryArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
     NSLog(@"%@",countryArray[row]);
    countryName.text=sortedCountryArray[row];
    countryName.textColor=[UIColor blackColor];
    
    countryDataPicker.hidden=YES;
    

}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 34;
}
@end
