//
//  SettingViewController.m
//  VIDIT
//
//  Created by brst on 4/6/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>

{
    NSArray *subMenu;
    NSArray *imgArr;
    UITableView *table;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Settings";
    self.navigationController.navigationBarHidden=NO;

    table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    table.delegate=self;
    table.dataSource=self;
    table.scrollEnabled=NO;
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:table];
   

    subMenu=@[@"Profile",@"Account",@"Chat Settings",@"Notification"];
    imgArr=@[@"setting1",@"setting2",@"setting3",@"setting4"];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];

    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
}
-(void)viewWillAppear:(BOOL)animated
{

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text=subMenu[indexPath.row];
    cell.imageView.image= [UIImage imageNamed:imgArr[indexPath.row]];
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
