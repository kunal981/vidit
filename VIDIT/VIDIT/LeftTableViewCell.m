//
//  LeftTableViewCell.m
//  VIDIT
//
//  Created by brst on 5/14/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "LeftTableViewCell.h"

@implementation LeftTableViewCell
@synthesize leftImage,timeLbl,msgLbl,leftMsg;


- (void)awakeFromNib {
    
   
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        leftImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 40, 40)];
        [self addSubview:leftImage];
        
        leftMsg=[[UIImageView alloc]initWithFrame:CGRectMake(45,20, self.contentView.frame.size.width-100, 40)];
        leftMsg.image=[UIImage imageNamed:@"leftBubble"];
        [self addSubview:leftMsg];
        
        msgLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.contentView.frame.size.width-105, 40)];
        msgLbl.font=[UIFont systemFontOfSize:12];
        msgLbl.numberOfLines=0;
        msgLbl.textColor=[UIColor grayColor];
        [leftMsg addSubview:msgLbl];
        
        timeLbl=[[UILabel alloc]initWithFrame:CGRectMake(leftMsg.frame.origin.x+leftMsg.frame.size.width+5, 20, 40, 40)];
        timeLbl.font=[UIFont systemFontOfSize:10];
        timeLbl.numberOfLines=1;
        timeLbl.textColor=[UIColor grayColor];
        [self addSubview:timeLbl];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
